﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GuestRegistrationSystem.Data;


namespace GuestRegistrationSystem.Controllers
{
    public class HomeController : Controller
    {
        private GuestRegSystemDBEntities db = new GuestRegSystemDBEntities();

        public ActionResult Index(int? eventId)
        {
            if (eventId != null)
            {
                foreach (var item in db.EventParticipants)
                {
                    if (item.EventID == eventId)
                    {
                        db.EventParticipants.Remove(item);
                    }
                }

                Event @event = db.Events.Find(eventId);
                db.Events.Remove(@event);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            var GetEvents = db.Events.OrderBy(x => x.Date);

            return View(GetEvents);
        }
        

        public ActionResult EventCreation()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EventCreation([Bind(Include = "EventID,EventName,Date,Location,Comment")] Event @event)
        {
            if (ModelState.IsValid)
            {
                if (@event.Date > DateTime.Now)
                {
                    db.Events.Add(@event);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.WrongDate = "Toimumisaeg ei saa olla minevikus! Palun sisesta korrektne kuupäev.";
            }

            return View(@event);
        }


        public ActionResult Participants(int? eventId, int? participantId)
        {
            if (eventId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Event @event = db.Events.Find(eventId);
            if (@event == null)
            {
                return HttpNotFound();
            }

            if (participantId != null)
            {
                var removedEvent = db.EventParticipants.Where(x => x.EventID == eventId && x.ParticipantID == participantId).Take(1).SingleOrDefault();
                db.EventParticipants.Remove(removedEvent);
                db.SaveChanges();
                return RedirectToAction("Participants", new { eventId = @event.EventID });
            }

            var PartCompPersonViewModel = new EventParticipantsModel
            {
                EventClass = @event
            };

            return View(PartCompPersonViewModel);
        }

        public class EventParticipantsModel
        {
            public Event EventClass { get; set; }
            public Participant ParticipantClass { get; set; }
            public Person PersonClass { get; set; }
            public Company CompanyClass { get; set; }

            public List<string> paymentTypes = new List<string> { "Sularaha", "Pangaülekanne" };
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Participants(EventParticipantsModel newParticipant)
        {
            db.Participants.Add(newParticipant.ParticipantClass);

            if (newParticipant.PersonClass.FName != null)
            {
                newParticipant.PersonClass.ParticipantID = newParticipant.ParticipantClass.ParticipantID;
                db.People.Add(newParticipant.PersonClass);
            }
            else
            {
                newParticipant.CompanyClass.ParticipantID = newParticipant.ParticipantClass.ParticipantID;
                db.Companies.Add(newParticipant.CompanyClass);
            }            

            EventParticipant addingParticipant = new EventParticipant()
            {
                EventID = newParticipant.EventClass.EventID,
                ParticipantID = newParticipant.ParticipantClass.ParticipantID
            };

            db.EventParticipants.Add(addingParticipant);
            
            db.SaveChanges();
            
            return RedirectToAction("Participants", new { eventId = newParticipant.EventClass.EventID });
        }


        // GET: Participant/5
        public ActionResult Participant(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Participant participant = db.Participants.Find(id);
            if (participant == null)
            {
                return HttpNotFound();
            }

            Person person = db.People.Where(x => x.ParticipantID == participant.ParticipantID).FirstOrDefault();
            Company company = db.Companies.Where(x => x.ParticipantID == participant.ParticipantID).FirstOrDefault();

            var PartCompPersonViewModel = new ParticipantDetailsModel
            {
                ParticipantClass = participant,
                PersonClass = person,
                CompanyClass = company
            };

            return View(PartCompPersonViewModel);
        }

        [HttpPost]
        public ActionResult Participant(ParticipantDetailsModel changedParticipant)
        {
            if (ModelState.IsValid)
            {
                db.Entry(changedParticipant.ParticipantClass).State = EntityState.Modified;

                if (changedParticipant.PersonClass != null)
                {
                    db.Entry(changedParticipant.PersonClass).State = EntityState.Modified;
                    db.SaveChanges();

                    return RedirectToAction("Participant", new { id = changedParticipant.ParticipantClass.ParticipantID });
                }
                else
                {
                    db.Entry(changedParticipant.CompanyClass).State = EntityState.Modified;
                    db.SaveChanges();

                    return RedirectToAction("Participant", new { id = changedParticipant.ParticipantClass.ParticipantID });
                }
            }
            
            return RedirectToAction("Index");
        }

        public class ParticipantDetailsModel
        {
            public Participant ParticipantClass { get; set; }
            public Person PersonClass { get; set; }
            public Company CompanyClass { get; set; }

            public List<string> paymentTypes = new List<string> { "Sularaha", "Pangaülekanne" };
        }
    }
}